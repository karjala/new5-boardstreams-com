#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';

chdir "$RealBin/perl";

exec 'carton exec -- morbo -w . -v scripts/my_app';
