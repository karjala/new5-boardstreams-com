package MyApp::Controller::WS;

use v5.36;
use Mojo::Base 'Mojolicious::Controller', -async_await;

use Crypt::PRNG 'rand', 'random_string_from';

async sub ws ($self) {
    my $cookie = $self->cookie('sid');
    my $user_id;
    if ($cookie) {{
        my ($_user_id, $_password) = split /\:/, $cookie;
        my $state = await $self->bs->get_state_p("users:$_user_id");
        $state or last;
        $_password eq $state->{_secret}{password} or last;
        $user_id = $_user_id;
    }}
    $user_id //= do {
        my $_user_id = random_string_from((join '', 'a'..'z'), 12);
        my $_password = random_string_from((join '', 'a'..'z'), 12);
        await $self->bs->create_stream_p("users:$_user_id", {
            name   => 'X-' . sprintf('%06d', int rand 1e6),
            _secret => { password => $_password },
        });
        $self->cookie(sid => "$_user_id:$_password", {
            path    => '/',
            expires => time + 10 * 365 * 24 * 3_600,
        });
        $_user_id;
    };
    $self->stash(user_id => $user_id);
}

1;
