package MyApp::Plugin::BSGlobal;

use v5.36;
use Mojo::Base 'Mojolicious::Plugin', -async_await;

sub register ($self, $app, $config) {
    $app->bs->create_stream('global', {
        online_users => {},
    });

    $app->bs->set_join('global', async sub ($c, @) {
        my $user_id = $c->user_id;
        my $worker_id = $c->bs->worker_id;
        await $c->bs->lock_stream_p('global', sub ($state) {
            $state->{online_users}{$user_id}{$worker_id}++;
            return undef, $state, 1;
        });
        return 1;
    });

    $app->bs->set_leave('global', async sub ($c, @) {
        my $user_id = $c->user_id;
        my $worker_id = $c->bs->worker_id;
        await $c->bs->lock_stream_p('global', sub ($state) {
            $state->{online_users}{$user_id}{$worker_id}--;
            if (! $state->{online_users}{$user_id}{$worker_id}) {
                delete $state->{online_users}{$user_id}{$worker_id};
                if (! keys $state->{online_users}{$user_id}->%*) {
                    delete $state->{online_users}{$user_id};
                }
            }
            return undef, $state, -1;
        });
    });

    # needed, because global's leave function tampers with global's state
    $app->bs->set_repair('global', async sub ($c, $stream_name, $get_dead_worker_ids) {
        await $c->bs->lock_stream_p($stream_name, async sub ($state) {
            my $dead_worker_ids = await $get_dead_worker_ids->();
            foreach my $user_id (keys $state->{online_users}->%*) {
                if (delete $state->{online_users}{$user_id}->@{keys %$dead_worker_ids}) {
                    if (! keys $state->{online_users}{$user_id}->%*) {
                        delete $state->{online_users}{$user_id};
                    }
                }
            }
            return undef, $state;
        });
    });

    $app->bs->set_request('global', whoami => sub ($c, @) {
        return $c->user_id;
    });

    $app->bs->set_action('global', rename_to => async sub ($c, $, $payload) {
        1 <= length $payload <= 15 or return;
        my $user_id = $c->user_id;
        await $c->bs->lock_stream_p("users:$user_id", sub ($state) {
            $state->{name} = $payload;
            return undef, $state;
        });
    });
}

1;
