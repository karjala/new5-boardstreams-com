package MyApp::Plugin::BSChat;

use v5.36;
use Mojo::Base 'Mojolicious::Plugin', -async_await;

use Time::HiRes ();

sub register ($self, $app, $config) {
    $app->bs->create_stream('chat', {
        users => {},
    });

    $app->bs->set_join('chat', async sub ($c, @) {
        my $user_id = $c->user_id;
        my $worker_id = $c->bs->worker_id;
        await $c->bs->lock_stream_p('chat', sub ($state) {
            $state->{users}{$user_id}{$worker_id}++;
            return undef, $state, 1;
        });
        return { limit => 20 };
    });

    $app->bs->set_leave('chat', async sub ($c, @) {
        my $user_id = $c->user_id;
        my $worker_id = $c->bs->worker_id;
        await $c->bs->lock_stream_p('chat', sub ($state) {
            $state->{users}{$user_id}{$worker_id}--;
            if (! $state->{users}{$user_id}{$worker_id}) {
                delete $state->{users}{$user_id}{$worker_id};
                if (! keys $state->{users}{$user_id}->%*) {
                    delete $state->{users}{$user_id};
                }
            }
            return undef, $state, -1;
        });
    });

    # needed, because chat's leave function tampers with chat's state
    $app->bs->set_repair('chat', async sub ($c, $stream_name, $get_dead_worker_ids) {
        await $c->bs->lock_stream_p($stream_name, async sub ($state) {
            my $dead_worker_ids = await $get_dead_worker_ids->();
            foreach my $user_id (keys $state->{users}->%*) {
                if (delete $state->{users}{$user_id}->@{keys %$dead_worker_ids}) {
                    if (! keys $state->{users}{$user_id}->%*) {
                        delete $state->{users}{$user_id};
                    }
                }
            }
            return undef, $state;
        });
    });

    $app->bs->set_request('chat', say => async sub ($c, $stream_name, $payload) {
        1 <= length $payload <= 200 or die "message length\n";
        my $user_id = $c->user_id;
        await $c->bs->lock_stream_p($stream_name, sub ($state) {
            my $event = {
                user_id => $user_id,
                message => $payload,
                time    => Time::HiRes::time() * 1e3,
            };
            return $event, undef;
        });
        return undef;
    });
}

1;
