package MyApp::Plugin::BSGrid;

use v5.36;
use Mojo::Base 'Mojolicious::Plugin', -async_await;

use Local::Util 'belongs_to';

no autovivification;

sub register ($self, $app, $config) {
    $app->bs->create_stream('grid', {
        users   => {},
        grid    => {},
        userPos => {},
    });

    $app->bs->set_join('grid', async sub ($c, @) {
        my $user_id = $c->user_id;
        my $worker_id = $c->bs->worker_id;
        await $c->bs->lock_stream_p('grid', sub ($state) {
            my $just_created = ! exists $state->{users}{$user_id};
            $state->{users}{$user_id}{$worker_id}++;
            if ($just_created) {
                my ($x, $y);
                for (1 .. 100) {
                    my ($_x, $_y) = ((1 + int rand 10), (1 + int rand 10));
                    if (! $state->{grid}{"$_x,$_y"}) {
                        $x = $_x;
                        $y = $_y;
                        last;
                    }
                }
                defined $x or return 0;
                $state->{userPos}{$user_id} = { x => $x, y => $y };
                $state->{grid}{"$x,$y"} = $user_id;
            }
            return undef, $state, 1;
        });
        return 1;
    });

    $app->bs->set_leave('grid', async sub ($c, @) {
        my $user_id = $c->user_id;
        my $worker_id = $c->bs->worker_id;
        await $c->bs->lock_stream_p('grid', sub ($state) {
            $state->{users}{$user_id}{$worker_id}--;
            if (! $state->{users}{$user_id}{$worker_id}) {
                delete $state->{users}{$user_id}{$worker_id};
                if (! keys $state->{users}{$user_id}->%*) {
                    delete $state->{users}{$user_id};
                    my $user_pos = delete $state->{userPos}{$user_id};
                    delete $state->{grid}{"$user_pos->{x},$user_pos->{y}"};
                }
            }
            return undef, $state, -1;
        });
    });

    # needed, because grid's leave function tampers with grid's state
    $app->bs->set_repair('grid', async sub ($c, $stream_name, $get_dead_worker_ids) {
        await $c->bs->lock_stream_p($stream_name, async sub ($state) {
            my $dead_worker_ids = await $get_dead_worker_ids->();
            foreach my $user_id (keys $state->{users}->%*) {
                if (delete $state->{users}{$user_id}->@{keys %$dead_worker_ids}) {
                    if (! keys $state->{users}{$user_id}->%*) {
                        delete $state->{users}{$user_id};
                        my $user_pos = delete $state->{userPos}{$user_id};
                        delete $state->{grid}{"$user_pos->{x},$user_pos->{y}"};
                    }
                }
            }
            return undef, $state;
        });
    });

    $app->bs->set_action('grid', move => async sub ($c, $stream_name, $payload) {
        # auth
        my $user_id = $c->user_id;

        # validate
        belongs_to($payload->{x}, [-1, 0, 1]) or die;
        belongs_to($payload->{y}, [-1, 0, 1]) or die;
        abs($payload->{x}) + abs($payload->{y}) == 1 or die;

        # act
        await $c->bs->lock_stream_p($stream_name, sub ($state) {
            my $pos = $state->{userPos}{$user_id};
            my $new_x = $pos->{x} + $payload->{x};
            my $new_y = $pos->{y} + $payload->{y};
            1 <= $new_x <= 10 or die;
            1 <= $new_y <= 10 or die;
            ! $state->{grid}{"$new_x,$new_y"} or die;

            delete $state->{grid}{"$pos->{x},$pos->{y}"};
            $state->{grid}{"$new_x,$new_y"} = $user_id;
            $state->{userPos}{$user_id} = {
                x => $new_x,
                y => $new_y,
            };

            return undef, $state;
        });
    });
}

1;
