package MyApp::Plugin::BSDemo1;

use v5.36;

use Mojo::Base 'Mojolicious::Plugin', -async_await;

sub register ($self, $app, $config) {
    $app->bs->create_stream('demo1', 0);

    $app->bs->set_join(demo1 => sub { 1 });

    $app->bs->set_action('demo1', inc => async sub ($c, $stream_name, $payload) {
        $c->bs->lock_stream_p($stream_name => sub ($state) {
            $state++;
            return undef, $state;
        });
    })
}

1;
