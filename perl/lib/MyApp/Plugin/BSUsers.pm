package MyApp::Plugin::BSUsers;

use v5.36;
use Mojo::Base 'Mojolicious::Plugin';

sub register ($self, $app, $config) {
    $app->bs->set_join(['users', qr/./], sub { 1 });
}

1;
