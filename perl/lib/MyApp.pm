package MyApp;

use v5.36;
use Mojo::Base 'Mojolicious';

use Mojo::Pg;

# This method will run once at server start
sub startup ($self) {

    # Load configuration from config file
    push $self->plugins->namespaces->@*, 'MyApp::Plugin';
    my $config = $self->plugin('Config');

    # Configure the application
    $self->secrets($config->{secrets});

    # BoardStreams
    my $pg = Mojo::Pg->new('postgresql://dbuser:dbuser@localhost/bs5');
    $self->plugin('BoardStreams', Pg => $pg);
    $self->plugin('BSDemo1');
    $self->plugin('BSGlobal');
    $self->plugin('BSUsers');
    $self->plugin('BSChat');
    $self->plugin('BSGrid');

    $self->helper(user_id => sub ($c) { $c->stash('user_id') });

    # Router
    my $r = $self->routes;

    $r->websocket('/ws')->to('WS#ws', 'boardstreams.endpoint' => 1);
}

1;