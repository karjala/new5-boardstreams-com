package Local::Util;

use v5.36;

use List::AllUtils 'any';

use experimental 'builtin';
use builtin 'refaddr';

use Exporter 'import';
our @EXPORT_OK = qw/ eqq belongs_to /;

sub eqq ($x, $y) {
    defined $x or return ! defined $y;
    defined $y or return !!0;
    ref($x) eq ref($y) or return !!0;
    length(ref $x) ? refaddr($x) == refaddr($y) : $x eq $y;
}

sub belongs_to ($x, $arr) {
    any { eqq($x, $_) } @$arr;
}

1;
