#!/usr/bin/env perl

use v5.36;
use FindBin '$RealBin';

chdir "$RealBin/html";

exec '. $NVM_DIR/nvm.sh && nvm use && npm run dev';
