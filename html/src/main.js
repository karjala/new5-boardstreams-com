import { createApp } from 'vue'
// import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import { GlobalEvents } from 'vue-global-events'
import ShowUser from './components/ShowUser.vue'

import './assets/style.scss'

const app = createApp(App)

// app.use(createPinia())
app.use(router)
app.component('GlobalEvents', GlobalEvents)
app.component('ShowUser', ShowUser)

app.mount('#app')
