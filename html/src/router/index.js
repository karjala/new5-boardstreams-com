import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Demo1 from '../views/Demo1.vue'
import Chat from '../views/Chat.vue'
import Grid from '../views/Grid.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/demo1',
      name: 'demo1',
      component: Demo1
    },
    {
      path: '/chat',
      name: 'chat',
      component: Chat,
    },
    {
      path: '/grid',
      name: 'grid',
      component: Grid,
    }
  ]
})

export default router
