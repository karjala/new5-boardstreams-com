import { ref } from 'vue';
import BSManager from 'boardstreams';

const BS = new BSManager('/ws');
BS.connect();
export default BS;
export let myUserId = ref(null);
